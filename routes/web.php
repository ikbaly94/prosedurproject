<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\workorder;

Route::get('/', function () {
    return view('template.demo_1.homeisi');
});

// workorder

Route::get('/work_order', 'workorder@home');
Route::get('/form', 'workorder@form');
